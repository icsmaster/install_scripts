#! /bin/bash

# created by Radim Janalik

SSHFS_VERSION="2.8"
WORKDIR="/apps/sshfs/${SSHFS_VERSION}/src"
INSTALLDIR="/apps/sshfs/${SSHFS_VERSION}"

# load gcc 6.1.0
module load gcc/6.1.0

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
if [ ! -f sshfs-${SSHFS_VERSION}.tar.gz ]; then 
   wget https://github.com/libfuse/sshfs/releases/download/sshfs_${SSHFS_VERSION}/sshfs-${SSHFS_VERSION}.tar.gz
fi
tar -zxf sshfs-${SSHFS_VERSION}.tar.gz

# create the build directory
mkdir -pv build
cd build

# build
../sshfs-${SSHFS_VERSION}/configure --prefix=${INSTALLDIR}
make -j 8 && make install

mkdir -pv /apps/third-party/modulefiles/sshfs
cd /apps/third-party/modulefiles/sshfs
cat >${SSHFS_VERSION}<<EOF
#%Module 3.2.0#####################################################################
##
## modules sshfs-${SSHFS_VERSION}
##
## modulefiles/gcc/sshfs-${SSHFS_VERSION}  Written by Radim Janalik
##
proc ModulesHelp {   } {
  global version modroot
    puts stderr "sshfs-${SSHFS_VERSION} - sets the Environment for sshfs ${SSHFS_VERSION}"

}

module-whatis  "Sets the environment for using sshfs-${SSHFS_VERSION}"

set SSHFS_DIR /apps/sshfs/${SSHFS_VERSION}

prepend-path PATH               \$SSHFS_DIR/bin
prepend-path MANPATH            \$SSHFS_DIR/share/man
EOF
