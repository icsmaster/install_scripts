#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# CMAKE VERSION
CMAKE_VERSION="3.5.2"

# OPENBLAS VERSION
OPENBLAS_VERSION="0.2.18${GCC_VERSION_M}"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load cmake/${CMAKE_VERSION}
module load openblas/${OPENBLAS_VERSION}

# NAME OF THE APPLICATION
APP_NAME="sundials"

# VERSION OF THE APPLICATION
APP_VERSION="2.6.2"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://computation.llnl.gov/projects/sundials-suite-nonlinear-differential-algebraic-equation-solvers/download/${APP_NAME}-${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf)
ARC_TYPE="xzf"

# CONFIGURATION
APP_CONF=" "

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS="-D CMAKE_INSTALL_PREFIX=${INSTALLDIR} -D CMAKE_BUILD_TYPE:STRING=Release \
		   -D FCMIX_ENABLE:BOOL=ON -D F90_ENABLE:BOOL=ON -D LAPACK_ENABLE:BOOL=ON -D MPI_ENABLE:BOOL=ON "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77                                  
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

# create the build directory
mkdir -pv ${INSTALLDIR}/build
cd build

cmake ${APP_FLAGS} ${WORKDIR}/${APP_NAME}-${APP_VERSION}
  
# make
make

# make install
make install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load openblas/0.2.18${GCC_VERSION_M}
module load cmake/3.5.2

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 SUNDIALS_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 CPATH	${INSTALLDIR}/include 

###############################################
EOF
