#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# CMAKE VERSION
CMAKE_VERSION="3.5.2"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2${GCC_VERSION_M}"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load cmake/${CMAKE_VERSION}
module load openmpi/${OPENMPI_VERSION}

# NAME OF THE APPLICATION
APP_NAME="parmetis"

# VERSION OF THE APPLICATION
APP_VERSION="4.0.3"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis/${APP_NAME}-${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xzf"

# DIRECTORIES
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"
WORKDIR="${INSTALLDIR}/src"
BUILDDIR="${WORKDIR}/build"
BINDIR="${INSTALLDIR}/bin"
INCDIR="${INSTALLDIR}/include"
LIBDIR="${INSTALLDIR}/lib"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS=" --prefix=${INSTALLDIR} --enable-shared --enable-parmetis-4 "

###############################################

# generate the work directory
mkdir -pv ${WORKDIR}
mkdir -pv ${BUILDDIR}
mkdir -pv ${BINDIR}
mkdir -pv ${INCDIR}
mkdir -pv ${LIBDIR}
cd ${WORKDIR}

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

export CC=mpicc
export CXX=mpicxx
export CPPFLAGS="-I /apps/hdf5/1.8.17/gcc-6.1.0/include"
export LDFLAGS="-L /apps/hdf5/1.8.17/gcc-6.1.0/lib"

###############################################

cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}

# build
./configure ${APP_FLAGS}

cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}

# make & install
make config prefix=${INSTALLDIR} shared=1 
make
make install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load cmake/3.5.2

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 PARMETIS_DIR 	${INSTALLDIR}

###############################################
###	        	  SET PATHS:
###############################################

prepend-path	 CPATH			${INSTALLDIR}/include 
prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib
prepend-path	 LD_LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 PATH	${INSTALLDIR}/bin  

###############################################
EOF
