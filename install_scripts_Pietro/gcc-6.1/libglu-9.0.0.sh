#!/bin/bash
 
##############################################

# DOWNLOAD AND EXTRACT:

# ftp://ftp.freedesktop.org/pub/mesa/glu/glu-9.0.0.tar.gz

##############################################

#make -j4 distclean # if in an existing build
 
autoreconf -fi
 
./configure \
     --prefix=/apps/mesa/gcc-6.1.0/llvmpipe
 
make -j2 
make -j4 install
