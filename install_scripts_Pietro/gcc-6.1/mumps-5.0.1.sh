#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2${GCC_VERSION_M}"

# SCOTCH VERSION
SCOTCH_VERSION="6.0.4${GCC_VERSION_M}"

# METIS VERSION
METIS_VERSION="5.1.0${GCC_VERSION_M}"

# PARMETIS VERSION
PARMETIS_VERSION="4.0.3${GCC_VERSION_M}"

# SCALAPACK VERSION
SCALAPACK_VERSION="2.0.2${GCC_VERSION_M}"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}
module load scotch/${SCOTCH_VERSION}
module load metis/${METIS_VERSION}
module load parmetis/${PARMETIS_VERSION}
module load scalapack/${SCALAPACK_VERSION}

# NAME OF THE APPLICATION
APP_NAME="mumps"

# VERSION OF THE APPLICATION
APP_VERSION="5.0.1"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://mumps.enseeiht.fr/MUMPS_${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="MUMPS_${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xzf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS=" "      

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################  

###############################################

# cd into extracted source directory
cd ${WORKDIR}/MUMPS_5.0.1

###############################################

cat <<EOF > Makefile.inc
SCOTCHDIR = $SCOTCH_DIR
ISCOTCH    = -I\$(SCOTCHDIR)/include
LSCOTCH    = -L\$(SCOTCHDIR)/lib -lptesmumps -lesmumps -lptscotch -lscotch -lscotcherr -lz -lm

LPORDDIR  = \$(topdir)/PORD/lib
IPORD     = -I\$(topdir)/PORD/include
LPORD     = -L\$(LPORDDIR) -lpord

IMETIS    = -I$METIS_DIR/include -I$PARMETIS_DIR/include
LMETIS    = -L$METIS_DIR/lib -lmetis -L$PARMETIS_DIR/lib -lparmetis

ORDERINGSF = -Dpord -Dparmetis -Dmetis -Dscotch -Dptscotch
ORDERINGSC = \$(ORDERINGSF)

LORDERINGS = \$(LSCOTCH) \$(LPORD) \$(LMETIS)
IORDERINGSF = \$(ISCOTCH)
IORDERINGSC = \$(ISCOTCH) \$(IMETIS) \$(IPORD)

PLAT    =
LIBEXT  = .a
OUTC    = -o
OUTF    = -o
RM = /bin/rm -f
CC = mpicc
FC = mpif90
FL = mpif90
AR = ar vr 
RANLIB = ranlib
SCALAP = -L$SCALAPACK_DIR/lib -lscalapack
INCPAR =
LIBPAR = \$(SCALAP)
# See point 17 in the FAQ to have more details on the compilation of mpich with gfortran
INCSEQ =
LIBSEQ =
LIBBLAS = -framework Accelerate
LIBOTHERS = -lm
#Preprocessor defs for calling Fortran from C (-DAdd_ or -DAdd__ or -DUPPER)
CDEFS   = -DAdd_

#Begin Optimized options
OPTF    = -fPIC -O3 -DALLOW_NON_INIT
OPTL    = -fPIC -O3
OPTC    = -fPIC -O3
#End Optimized options
INCS = \$(INCPAR)
LIBS = \$(LIBPAR)
LIBSEQNEEDED =
EOF

###############################################

# make
make -j 12 alllib

# install
for dir in {lib,include}; do
    install -d ${INSTALLDIR}/$dir
    install -m 755 ${WORKDIR}/MUMPS_5.0.1/$dir/* ${INSTALLDIR}/$dir
done

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load openmpi/1.10.2${GCC_VERSION_M}  
module load scotch/6.0.4${GCC_VERSION_M}
module load parmetis/4.0.3${GCC_VERSION_M}
module load metis/5.1.0${GCC_VERSION_M}
module load scalapack/2.0.2${GCC_VERSION_M}

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-5.3 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

set MUMPS_DIR ${INSTALLDIR}
setenv MUMPS_DIR \$MUMPS_DIR

###############################################
###	        	  PATHS:
###############################################

prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 CPATH	${INSTALLDIR}/include 

###############################################
EOF
