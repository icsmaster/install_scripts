#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2${GCC_VERSION_M}"

# METIS VERSION
METIS_VERSION="5.1.0${GCC_VERSION_M}"

# PARMETIS VERSION
PARMETIS_VERSION="4.0.3${GCC_VERSION_M}"

# SCALAPACK VERSION
SCALAPACK_VERSION="2.0.2${GCC_VERSION_M}"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}
module load metis/${METIS_VERSION}
module load parmetis/${PARMETIS_VERSION}
module load scalapack/${SCALAPACK_VERSION}

# NAME OF THE APPLICATION
APP_NAME="petsc"

# VERSION OF THE APPLICATION
APP_VERSION="3.6.4"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="petsc-${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf)
ARC_TYPE="xzf"

# CONFIGURATION
APP_CONF="${APP_NAME}-${APP_VERSION}/configure"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
#APP_FLAGS="--prefix=${WORKDIR}/${APP_NAME}-${APP_VERSION} --download-hypre=1 --with-ssl=0 --with-debugging=no --with-pic=1 --with-shared-libraries=1 --with-cc=mpicc --with-cxx=mpicxx --with-fc=mpif90 --with-parmetis-include=${PARMETIS_DIR}/include --with-parmetis-lib=\"-L${PARMETIS_DIR}/lib -lparmetis\" --with-blas-lapack-lib=\"-L/usr/lib -lblas -llapack\" --with-metis-include=${METIS_DIR}/include --with-metis-lib=[${METIS_DIR}/lib/libmetis.so] --download-superlu_dist=1 --with-scalapack-include=${SCALAPACK_DIR}/include --with-scalapack-lib=\"-L${SCALAPACK_DIR}/lib -lscalapack\" --download-mumps=1"

# Notes
#   --parallel=n
#       bootstrap cmake in parallel, where n is
#       number of nodes [1]
#
#   --no-system-libs
#       use all system-installed third-party libraries
#       (for use only by package maintainers).
#
#   --no-qt-gui
#       do not build the Qt-based GUI (default)

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

# create the build directory
mkdir -pv ${WORKDIR}/build
cd build

cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}

# build
./configure --prefix=${INSTALLDIR} --download-hypre=1 --with-ssl=0 --with-debugging=no \
--with-pic=1 --with-shared-libraries=1 --with-cc=mpicc --with-cxx=mpicxx --with-fc=mpif90 \
--with-parmetis-include=${PARMETIS_DIR}/include \
--with-parmetis-lib="-L${PARMETIS_DIR}/lib -lparmetis -lmetis" \
--with-blas-lapack-lib="-L/usr/lib -lblas -llapack" \
--with-metis-include=${METIS_DIR}/include --with-metis-lib=[${METIS_DIR}/lib/libmetis.so] \
--download-superlu_dist=1 --with-scalapack-lib="-L${SCALAPACK_DIR}/lib -lscalapack" \
--download-mumps=1

# make & install
make && \
make install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load openmpi/1.10.2${GCC_VERSION_M} 
module load parmetis/4.0.3${GCC_VERSION_M}
module load metis/5.1.0${GCC_VERSION_M}
module load scalapack/2.0.2${GCC_VERSION_M}

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-5.3 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION} and openmpi-${OPENMPI_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv PETSC_DIR ${INSTALLDIR}
setenv CC  mpicc
setenv CXX mpicxx
setenv F90 mpif90
setenv F77 mpif77
setenv FC  mpif90
set base_path \$PETSC_DIR

###############################################
###	        	  SET PATHS:
###############################################

prepend-path PATH \$PETSC_DIR/bin
prepend-path LD_LIBRARY_PATH \$PETSC_DIR/lib
prepend-path INCLUDE_PATH \$PETSC_DIR/include
prepend-path PKG_CONFIG_PATH \$PETSC_DIR/lib/pkgconfig

###############################################
EOF