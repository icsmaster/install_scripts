#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}

# NAME OF THE APPLICATION
APP_NAME="openmpi"

# VERSION OF THE APPLICATION
APP_VERSION="1.10.2"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="https://www.open-mpi.org/software/ompi/v1.10/downloads/${APP_NAME}-${APP_VERSION}.tar.bz2"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.bz2"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf)
ARC_TYPE="xjf"

# CONFIGURATION
APP_CONF="${APP_NAME}-${APP_VERSION}/configure"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS="--prefix=${INSTALLDIR} --enable-mpi-fortran=all"

# Notes
#   --parallel=n
#       bootstrap cmake in parallel, where n is
#       number of nodes [1]
#
#   --no-system-libs
#       use all system-installed third-party libraries
#       (for use only by package maintainers).
#
#   --no-qt-gui
#       do not build the Qt-based GUI (default)

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################



###############################################

# create the build directory
mkdir -pv ${WORKDIR}/build
cd build

# build
../${APP_CONF} ${APP_FLAGS}

# make & install
make -j 12 && make install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/6.1.0

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0
conflict openmpi/1.10.2_gcc-5.3 
conflict openmpi/1.10.2_gcc-4.8 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 OPENMPI_DIR 	${INSTALLDIR}

###############################################
###	        	  SET PATHS:
###############################################

prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 LD_LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 CPATH	${INSTALLDIR}/include 
prepend-path	 PATH	${INSTALLDIR}/bin 

###############################################
EOF
