#! /bin/bash

# created by Xiaozhou Li and updated by Pietro Bressana
# load gcc version you would like to compile this module with

GCC_VERSION="6.1.0"

module load cmake
module load gcc/${GCC_VERSION}
module load openmpi/1.10.2_gcc-6.1

SCALAPACK_VERSION="2.0.2"
GCC_VERSION_M="_gcc-6.1"
WORKDIR="/apps/scalapack/${SCALAPACK_VERSION}${GCC_VERSION_M}/src"
INSTALLDIR="/apps/scalapack/${SCALAPACK_VERSION}${GCC_VERSION_M}"
MPI_BASE_DIR="${OPENMPI_DIR}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f scalapack-${SCALAPACK_VERSION}.tgz ]; then
wget http://www.netlib.org/scalapack/scalapack-${SCALAPACK_VERSION}.tgz
fi

tar zxvf scalapack-${SCALAPACK_VERSION}.tgz

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77                                  
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

# create the build directory
#mkdir -pv ${WORKDIR}/build
#cd build

# build with cmake for shared library

cd scalapack-${SCALAPACK_VERSION}
mkdir build
cd build 
cmake -D BUILD_SHARED_LIBS:BOOL=ON -D CMAKE_INSTALL_PREFIX=${INSTALLDIR} $WORKDIR/scalapack-${SCALAPACK_VERSION}
make -j 10
make install

##############################
#for module files
##############################

# create module file directory
mkdir -pv /apps/third-party/modulefiles/scalapack
cd /apps/third-party/modulefiles/scalapack

# create module file
cat >${SCALAPACK_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
#
# modules scalapack-${SCALAPACK_VERSION}${GCC_VERSION_M}
#
# modulefiles/scalapack/${SCALAPACK_VERSION}${GCC_VERSION_M}  Written by Xiaozhou Li and updated by Pietro Bressana
#

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load cmake
module load gcc/${GCC_VERSION}
module load openmpi/1.10.2_gcc-6.1

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-5.3 

###############################################

proc ModulesHelp {   } {
  global version modroot
      puts stderr "scalapack-${SCALAPACK_VERSION}{GCC_VERSION_M} - sets the Environment for
      SCALAPACK ${SCALAPACK_VERSION}{GCC_VERSION_M}"
}

module-whatis  "Sets the environment for using SCALAPACK-${SCALAPACK_VERSION}{GCC_VERSION_M}
built with gcc-${GCC_VERSION}"

set SCALAPACK_DIR ${INSTALLDIR}
setenv SCALAPACK_DIR               \$SCALAPACK_DIR

prepend-path LD_LIBRARY_PATH    \$SCALAPACK_DIR/lib

EOF