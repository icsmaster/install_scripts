#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}

# NAME OF THE APPLICATION
APP_NAME="pigz"

# VERSION OF THE APPLICATION
APP_VERSION="2.3.3"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://zlib.net/pigz/${APP_NAME}-${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xzf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

###############################################

# cd into installdir
cd ${INSTALLDIR}

# generate bin directory
mkdir -pv bin

# cd into extracted source directory
cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}

# make
make

# install
cp pigz ${INSTALLDIR}/bin
cp unpigz ${INSTALLDIR}/bin

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 PIGZ_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path     PATH   ${INSTALLDIR}/bin 
 
###############################################
EOF
