#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2${GCC_VERSION_M}"

# CMAKE VERSION
CMAKE_VERSION="3.5.2"

# OPENBLAS VERSION
OPENBLAS_VERSION="0.2.18${GCC_VERSION_M}"

# PARMETIS VERSION
PARMETIS_VERSION="4.0.3${GCC_VERSION_M}"

# METIS VERSION
METIS_VERSION="5.1.0${GCC_VERSION_M}"

# NETCDF VERSION
NETCDF_VERSION="4.4.0"

# SUPERLU VERSION
SUPERLU_VERSION="5.2.1${GCC_VERSION_M}"

# HDF5 VERSION
HDF5_VERSION="1.8.17${GCC_VERSION_M}"

# SCALAPACK VERSION
SCALAPACK_VERSION="2.0.2${GCC_VERSION_M}"

# SCOTCH VERSION
SCOTCH_VERSION="6.0.4${GCC_VERSION_M}"

# EIGEN VERSION
EIGEN_VERSION="3.2.8${GCC_VERSION_M}"

# HYPRE VERSION
HYPRE_VERSION="2.11.1${GCC_VERSION_M}"

# MUMPS VERSION
MUMPS_VERSION="5.0.1${GCC_VERSION_M}"

# MATIO VERSION
MATIO_VERSION="1.5.2${GCC_VERSION_M}"

# SWIG VERSION
SWIG_VERSION="3.0.10${GCC_VERSION_M}"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}
MPI_BASE_DIR="${OPENMPI_DIR}"
module load cmake/${CMAKE_VERSION}
module load openblas/${OPENBLAS_VERSION}
module load parmetis/${PARMETIS_VERSION}
module load metis/${METIS_VERSION}
module load netcdf/${NETCDF_VERSION}
module load superlu/${SUPERLU_VERSION}
module load hdf5/${HDF5_VERSION}
module load scalapack/${SCALAPACK_VERSION}
module load scotch/${SCOTCH_VERSION}
module load eigen/${EIGEN_VERSION}
module load hypre/${HYPRE_VERSION}
module load mumps/${MUMPS_VERSION}
module load matio/${MATIO_VERSION}
module load swig/${SWIG_VERSION}

# NAME OF THE APPLICATION
APP_NAME="trilinos"

# VERSION OF THE APPLICATION
APP_VERSION="12.6.3"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://trilinos.csbsju.edu/download/files/trilinos-12.6.3-Source.tar.bz2"

# NAME OF THE DOWNLOADED FILE
APP_FILE="trilinos-12.6.3-Source.tar.bz2"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xjf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
BUILDDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/build"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" -D CMAKE_INSTALL_PREFIX=${INSTALLDIR} -D CMAKE_BUILD_TYPE=Release \
			-D Trilinos_ENABLE_ALL_PACKAGES=ON -D Trilinos_ENABLE_ALL_OPTIONAL_PACKAGES=ON \
            -D Trilinos_ENABLE_TESTS=OFF -D Trilinos_ENABLE_EXAMPLES:BOOL=OFF \
            -D Trilinos_ENABLE_FEI=OFF -D Trilinos_ENABLE_Pike=OFF \
            -D Trilinos_ENABLE_Piro=OFF -D Trilinos_ENABLE_SEACAS=OFF \
            -D Trilinos_ENABLE_STK=OFF -D Trilinos_ENABLE_Stokhos=OFF \
            -D Trilinos_ENABLE_Sundance=OFF -D Trilinos_ENABLE_Zoltan2=OFF
            -D Trilinos_ENABLE_Amesos2=OFF \
            -D Teuchos_ENABLE_COMPLEX:BOOL=OFF -D KokkosTSQR_ENABLE_Complex:BOOL=OFF \
			-D Trilinos_ENABLE_CXX11=ON -D TPL_ENABLE_MPI=ON -D BUILD_SHARED_LIBS=ON \
            -D TPL_ENABLE_BLAS=ON -D TPL_ENABLE_METIS:BOOL=ON -D TPL_ENABLE_ParMETIS:BOOL=ON \
            -D TPL_ENABLE_Netcdf:BOOL=ON -D TPL_ENABLE_HDF5:BOOL=ON \          
            -D TPL_ENABLE_SCALAPACK:BOOL=ON -D TPL_SCALAPACK_LIBRARIES=${SCALAPACK_DIR}/lib \ 
            -D TPL_Matio_LIBRARIES=${MATIO_DIR}/lib -D TPL_Matio_INCLUDE_DIRS=${MATIO_DIR}/include \
            -D TPL_ENABLE_HYPRE:BOOL=ON -D TPL_HYPRE_LIBRARIES=${HYPRE_DIR}/lib -D TPL_HYPRE_INCLUDE_DIRS=${HYPRE_DIR}/include "

# -D Trilinos_ENABLE_<TRIBITS_PACKAGE>=OFF          
# -D TPL_ENABLE_Eigen:BOOL=ON -D TPL_Eigen_INCLUDE_DIRS=${EIGEN_DIR}/include \
# -D TPL_ENABLE_SuperLU:BOOL=ON -D TPL_SuperLU_LIBRARIES=${SUPERLU_DIR}/lib -D TPL_SuperLU_INCLUDE_DIRS=${SUPERLU_DIR}/include \
# -D TPL_ENABLE_Scotch:BOOL=ON -D TPL_Scotch_LIBRARIES=${SCOTCH_DIR}/lib -D TPL_Scotch_INCLUDE_DIRS=${SCOTCH_DIR}/include \
# -D Trilinos_ENABLE_Teuchos=OFF -D Trilinos_ENABLE_TriUtils=OFF \
# -D Trilinos_ENABLE_IFPACK=OFF \

###############################################

# generate the build directory
mkdir -pv $BUILDDIR

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77                                  
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

# cd into build directory
cd $BUILDDIR

# cmake configuration
cmake ${APP_FLAGS} ${WORKDIR}/trilinos-12.6.3-Source

# cd into build directory
cd $BUILDDIR

# make all targets
make -j 12

# run tests
ctest -j 12

# install
make -j 12 install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load openmpi/1.10.2${GCC_VERSION_M}  
module load cmake/3.5.2
module load openblas/0.2.18${GCC_VERSION_M}
module load parmetis/4.0.3${GCC_VERSION_M}
module load metis/5.1.0${GCC_VERSION_M}
module load netcdf/4.4.0
module load superlu/5.2.1${GCC_VERSION_M}
module load hdf5/1.8.17${GCC_VERSION_M}
module load scalapack/2.0.2${GCC_VERSION_M}
module load scotch/6.0.4${GCC_VERSION_M}
module load eigen/3.2.8${GCC_VERSION_M}
module load hypre/2.11.1${GCC_VERSION_M}

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-5.3 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 TRILINOS_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 CPATH	${INSTALLDIR}/include 
prepend-path	 PATH	${INSTALLDIR}/bin 

###############################################
EOF
