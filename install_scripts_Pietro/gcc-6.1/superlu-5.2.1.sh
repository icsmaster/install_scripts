#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2${GCC_VERSION_M}"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}

# NAME OF THE APPLICATION
APP_NAME="superlu"

# VERSION OF THE APPLICATION
APP_VERSION="5.2.1"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://crd-legacy.lbl.gov/~xiaoye/SuperLU/${APP_NAME}_${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}_${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf)
ARC_TYPE="xzf"

# CONFIGURATION
APP_CONF="${APP_NAME}-${APP_VERSION}/configure"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS=""

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

cd ${WORKDIR}/SuperLU_5.2.1

###############################################

cat <<EOF > make.inc
SuperLUroot = ${WORKDIR}/SuperLU_5.2.1
SUPERLULIB  = \$(SuperLUroot)/lib/libsuperlu.a
TMGLIB      = libtmglib.a

BLASDEF   = -DUSE_VENDOR_BLAS
BLASLIB   = \$(SuperLUroot)/lib/libblas.a

LIBS = \$(SUPERLULIB) \$(BLASLIB)

ARCH         = ar
ARCHFLAGS    = cr
RANLIB       = echo

CC           = cc
CFLAGS       = -DPRNTlevel=0 -O3 -march=native -fPIC
NOOPTS       = -O0 -fPIC
FORTRAN      = gfortran
FFLAGS       = -O3 -march=native -fPIC
LOADER       = \$(CC)
LOADOPTS     = -Wa,-q
CDEFS        = -DAdd_

MATLAB = /apps/matlab
EOF

###############################################

# make clean
make cleanlib

# make libsuperlu
make lib

# make CBLAS library
cd ${WORKDIR}/SuperLU_5.2.1/CBLAS
make

# testing
#cd ${WORKDIR}/SuperLU_5.2.1/TESTING
#make

# remove old installation
install -d ${INSTALLDIR}/lib
install -m 755 ${WORKDIR}/SuperLU_5.2.1/lib/libsuperlu.a ${INSTALLDIR}/lib
install -m 755 ${WORKDIR}/SuperLU_5.2.1/lib/libblas.a ${INSTALLDIR}/lib
install -d ${INSTALLDIR}/include
install -m 644 ${WORKDIR}/SuperLU_5.2.1/SRC/*.h ${INSTALLDIR}/include
rm -v ${INSTALLDIR}/include/{html_mainpage,colamd}.h

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load openmpi/1.10.2${GCC_VERSION_M} 

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-5.3 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

set SUPERLU_DIR ${INSTALLDIR}
setenv SUPERLU_DIR \$SUPERLU_DIR

###############################################
###	        	  SET PATHS:
###############################################

###############################################
EOF
