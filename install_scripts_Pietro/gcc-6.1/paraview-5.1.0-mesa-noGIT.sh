#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# CMAKE VERSION
CMAKE_VERSION="3.5.2"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2${GCC_VERSION_M}"

# PYTHON VERSION
PYTHON_VERSION="3.5.0"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load cmake/${CMAKE_VERSION}
module load openmpi/${OPENMPI_VERSION}
module load python/${PYTHON_VERSION}

# NAME OF THE APPLICATION
APP_NAME="paraview"

# VERSION OF THE APPLICATION
APP_VERSION="5.1.0-mesa"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO=" "

# NAME OF THE DOWNLOADED FILE
APP_FILE=" "

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xzf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"
DWLDIR="${WORKDIR}/download"
BUILDDIR="${WORKDIR}/build"
MESADIR="/apps/mesa/gcc-6.1.0/llvmpipe"

# SET MESA LIBRARY_PATH
LD_LIBRARY_PATH="${MESADIR}/lib:${LD_LIBRARY_PATH}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" -D CMAKE_INSTALL_PREFIX=${INSTALLDIR} -D CMAKE_BUILD_TYPE:STRING=Release \
			-D BUILD_SHARED_LIBS:BOOL=ON -D download_location=${WORKDIR}/download \
			-D ENABLE_paraview:BOOL=ON\
			-D ENABLE_mpi:BOOL=ON -D USE_SYSTEM_mpi:BOOL=ON \
			-D MPI_C_LIBRARIES=/apps/openmpi/1.10.2/gcc-5.3.0/lib \
			-D MPI_C_INCLUDE_PATH=/apps/openmpi/1.10.2/gcc-5.3.0/include \
			-D CMAKE_C_FLAGS=-lmpi \
			-D ENABLE_osmesa:BOOL=ON \
			-D PARAVIEW_RENDERING_BACKEND=OpenGL2 \
			-D ParaView_FROM_GIT:BOOL=OFF \
			-D ParaView_FROM_SOURCE_DIR:BOOL=ON "

#			-D ENABLE_mesa:BOOL=ON \
#			-D PARAVIEW_BUILD_QT_GUI:BOOL=OFF \
#		    -D VTK_USE_X:BOOL=OFF \
# 		    -D VTK_USE_MPI:BOOL=ON \
# 		    -D VTK_USE_OSMESA:BOOL=ON \
# 		    -D VTK_OPENGL_HAS_OSMESA=ON \
#		    -D OPENGL_INCLUDE_DIR=${MESADIR}/include \
#		    -D OPENGL_gl_LIBRARY=${MESADIR}/lib/libOSMesa.so \
#		    -D OPENGL_glu_LIBRARY=${MESADIR}/lib/libGLU.so \
#		    -D OSMESA_INCLUDE_DIR=${MESADIR}/include \
#		    -D OSMESA_LIBRARY=${MESADIR}/lib/libOSMesa.so "

###############################################

# generate the work directory
mkdir -pv ${WORKDIR}
mkdir -pv ${DWLDIR}
mkdir -pv ${BUILDDIR}
cd ${WORKDIR}

cp -r ParaViewSource ${BUILDDIR}

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77                                  
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

# cd into build directory
cd ${WORKDIR}/build

# cmake
cmake ${APP_FLAGS} ${WORKDIR}/ParaViewSuperbuild

# cd into build directory
cd ${WORKDIR}/build

# make
make -j 12

# make install
make -j 12 install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load openmpi/1.10.2${GCC_VERSION_M} 
module load cmake/3.5.2
module load python/3.5.0

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-5.3 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 PARAVIEW_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 PATH	${INSTALLDIR}/bin 
prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
 
###############################################
EOF
