#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# CMAKE VERSION
CMAKE_VERSION="3.5.2"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load cmake/${CMAKE_VERSION}

# NAME OF THE APPLICATION
APP_NAME="eigen"

# VERSION OF THE APPLICATION
APP_VERSION="3.2.8"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="https://bitbucket.org/eigen/eigen/get/${APP_VERSION}.tar.bz2"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_VERSION}.tar.bz2"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf)
ARC_TYPE="xjf"

# CONFIGURATION
APP_CONF="${APP_NAME}-${APP_VERSION}/configure"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS="-D CMAKE_BUILD_TYPE:STRING=Release /
-D CMAKE_INSTALL_PREFIX=${INSTALLDIR} /
-D BUILD_SHARED_LIBS:BOOL=ON /
-D CMAKE_SKIP_INSTALL_RPATH:BOOL=OFF /
-D CMAKE_SKIP_BUILD_RPATH:BOOL=OFF /
-D CMAKE_INSTALL_NAME_DIR:PATH=${INSTALLDIR}/lib"

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

###############################################

# create the build directory
mkdir -pv ${INSTALLDIR}/build
cd build

cmake ${APP_FLAGS} ${WORKDIR}/eigen-eigen-07105f7124f9
  
# make install
make install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load cmake/${CMAKE_VERSION}

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

set EIGEN_DIR ${INSTALLDIR}
setenv EIGEN_DIR \$EIGEN_DIR

###############################################
###	        	  SET PATHS:
###############################################

prepend-path PKG_CONFIG_PATH \$EIGEN_DIR/lib/pkgconfig

###############################################
EOF
