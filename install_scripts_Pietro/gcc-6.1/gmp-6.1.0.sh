#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}

# NAME OF THE APPLICATION
APP_NAME="gmp"

# VERSION OF THE APPLICATION
APP_VERSION="6.1.0"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://ftpmirror.gnu.org/gmp/${APP_NAME}-${APP_VERSION}.tar.bz2"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.bz2"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xjf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" --prefix=${INSTALLDIR} --enable-cxx "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

###############################################

# cd into extracted source directory
cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}

# configure
./configure ${APP_FLAGS}

# make
make

# make install
make install

# install
# cp pigz ${INSTALLDIR}/bin

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 GMP_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib
prepend-path	 CPATH	${INSTALLDIR}/include 
 
###############################################
EOF
