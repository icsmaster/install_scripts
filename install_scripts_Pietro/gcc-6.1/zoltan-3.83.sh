#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2${GCC_VERSION_M}"

# SCOTCH VERSION
SCOTCH_VERSION="6.0.4${GCC_VERSION_M}"

# PARMETIS VERSION
PARMETIS_VERSION="4.0.3${GCC_VERSION_M}"

# METIS VERSION
METIS_VERSION="5.1.0${GCC_VERSION_M}"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}
module load scotch/${SCOTCH_VERSION}
module load parmetis/${PARMETIS_VERSION}
module load metis/${METIS_VERSION}

# NAME OF THE APPLICATION
APP_NAME="zoltan"

# VERSION OF THE APPLICATION
APP_VERSION="3.83"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://www.cs.sandia.gov/~kddevin/Zoltan_Distributions/${APP_NAME}_distrib_v${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}_distrib_v${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf)
ARC_TYPE="xzf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS=" --prefix=${INSTALLDIR} --with-scotch \
			--with-scotch-libdir=${SCOTCH_DIR}/lib  --with-scotch-incdir=${SCOTCH_DIR}/include \
			--with-parmetis --with-parmetis-libdir=${PARMETIS_DIR}/lib \
			--with-parmetis-incdir=${PARMETIS_DIR}/include \
			--enable-f90interface "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=gfortran
#FC=mpif90
F77=mpif77
F90=gfortran                                  
#F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

cd $WORKDIR

# configure
./Zoltan_v3.83/configure ${APP_FLAGS}

cd $WORKDIR

# make
make everything

# check
make check

# install
make install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load openmpi/1.10.2${GCC_VERSION_M}  
module load scotch/6.0.4${GCC_VERSION_M}
module load parmetis/4.0.3${GCC_VERSION_M}
module load metis/5.1.0${GCC_VERSION_M}

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0
conflict openmpi/1.10.2_gcc-4.8 
conflict openmpi/1.10.2_gcc-5.3 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 ZOLTAN_DIR 	${INSTALLDIR}

###############################################
###	        	  SET PATHS:
###############################################

prepend-path	 CPATH			${ZOLTAN_DIR}/include 
prepend-path	 LIBRARY_PATH	${ZOLTAN_DIR}/lib 

###############################################
EOF
