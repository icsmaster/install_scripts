#! /bin/bash

###############################################
###           script_template.sh
###############################################

# Created by &ltAUTHOR&gt

###############################################
###	        REQUIRED INFORMATION:
###############################################

# COMPILER VERSION
COMPILER_VERSION="&ltVERSION_NUMBER&gt"
COMPILER_VERSION_M="_&ltCOMPILER_NAME&gt-&ltVERSION_NUMBER&gt"

# OTHER REQUIRED APPLICATION VERSION
OTHER_APP_VERSION="&ltVERSION_NUMBER&gt${COMPILER_VERSION_M}"

# REQUIRED MODULES:
module load &ltCOMPILER_NAME&gt/${COMPILER_VERSION}
module load &ltOTHER_APP_NAME&gt/${OTHER_APP_VERSION}

# NAME OF THE APPLICATION
APP_NAME="&ltNAME&gt"

# VERSION OF THE APPLICATION
APP_VERSION="&ltVERSION_NUMBER&gt"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="&ltWEB_ADDRES&gt"

# NAME OF THE DOWNLOADED FILE
APP_FILE="&ltFILE_NAME&gt"

# ARCHIVE FLAG TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xjf"

# DIRECTORIES
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/&ltCOMPILER_NAME&gt-${COMPILER_VERSION}"
WORKDIR="${INSTALLDIR}/src"
BUILDDIR="${WORKDIR}/build"
BINDIR="${INSTALLDIR}/bin"
INCDIR="${INSTALLDIR}/include"
LIBDIR="${INSTALLDIR}/lib"

# CMAKE or CONFIG FLAGS [!!! at least PREFIX and LIBS !!!]
APP_FLAGS=" "

###############################################

# generate directories
mkdir -pv ${WORKDIR}
mkdir -pv ${BUILDDIR}
mkdir -pv ${BINDIR}
mkdir -pv ${INCDIR}
mkdir -pv ${LIBDIR}

# cd to work directory
cd ${WORKDIR}

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###         usefull for mpi, fortran, ...
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77                                  
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

# cd into build directory
cd ${BUILDDIR}

# CONFIGURE USING EITHER CMAKE OR CONFIG
cmake ${APP_FLAGS} ${WORKDIR}/${APP_FILE}
./configure ${APP_FLAGS}

# cd into build directory
cd ${BUILDDIR}

# make all targets
make -j 12

# install
make -j 12 install

###############################################
###                 MODULEFILE
###############################################

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${COMPILER_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${COMPILER_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${COMPILER_VERSION_M}  Written by &ltAUTHOR&gt
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load &ltCOMPILER_NAME&gt/${COMPILER_VERSION}
module load &ltOTHER_APP_NAME&gt/${OTHER_APP_VERSION}

###############################################
###            CONFLICTING MODULES:
###############################################

conflict &ltMODULE_NAME&gt/&ltMODULE_VERSION&gt

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with &ltCOMPILER_NAME&gt-${COMPILER_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 APP_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 LIBRARY_PATH       ${INSTALLDIR}/lib 
prepend-path     LD_LIBRARY_PATH    ${INSTALLDIR}/lib 
prepend-path     PATH               ${INSTALLDIR}/bin 
prepend-path	 CPATH              ${INSTALLDIR}/include 
prepend-path     INCLUDE_PATH       ${INSTALLDIR}/include 

###############################################
EOF
