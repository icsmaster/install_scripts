###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0

###############################################

module load arpack/3.2.0
module load gcc/6.1.0
module load openmpi/1.10.2_gcc-5.3.0
module load openmpi/1.10.2_gcc-4.8.3
module load openmpi/1.10.2_gcc-6.1.0 
module load cmake/3.5.2
module load openblas/0.2.18
module load parmetis/4.0.3
module load metis/5.1.0
module load netcdf/4.4.0
module load superlu/5.2.1
module load hdf5/1.8.17
module load scalapack/2.0.2
module load scotch/6.0.4
module load eigen/3.2.8
module load hypre/2.11.1
module load mumps/5.0.1
module load matio/1.5.2
module load swig/3.0.10
module load python/3.5.0
module load gmp/6.1.0 

***********************************************

conflict gcc/5.3.0
conflict gcc/6.1.0
conflict openmpi/1.10.2_gcc-4.8.3
conflict openmpi/1.10.2_gcc-6.1.0 
conflict openmpi/1.10.2_gcc-5.3.0
conflict cmake/3.5.2
conflict openblas/0.2.18
conflict parmetis/4.0.3
conflict metis/5.1.0
conflict netcdf/4.4.0
conflict superlu/5.2.1
conflict hdf5/1.8.17
conflict scalapack/2.0.2
conflict scotch/6.0.4
conflict eigen/3.2.8
conflict hypre/2.11.1
conflict mumps/5.0.1
conflict matio/1.5.2
conflict swig/3.0.10
conflict gmp/gmp-6.1.0-gcc-4.8.3