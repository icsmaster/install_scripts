#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"
GCC_VERSION_M="_gcc-5.3"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2_gcc-5.3.0"

# OPENBLAS VERSION
OPENBLAS_VERSION="0.2.18"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}
module load openblas/${OPENBLAS_VERSION}

# NAME OF THE APPLICATION
APP_NAME="arpack"

# VERSION OF THE APPLICATION
APP_VERSION="3.2.0"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="https://github.com/opencollab/arpack-ng/archive/${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf)
ARC_TYPE="xzf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS=" --disable-dependency-tracking --prefix=${INSTALLDIR} \
			--enable-mpi --with-blas=-lblas "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77                                  
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

cd ${WORKDIR}/arpack-ng-3.2.0

# configure
./configure ${APP_FLAGS}

# make
make

# check
make check

# install
make install

cd ${INSTALLDIR}/lib

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0
module load openmpi/1.10.2_gcc-5.3
module load openblas/0.2.18_gcc-5.3

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0
conflict openmpi/1.10.2_gcc-4.8
conflict openmpi/1.10.2_gcc-6.1

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 ARPACK_DIR 	${INSTALLDIR}

###############################################
###	        	  SET PATHS:
###############################################

prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 

###############################################
EOF
