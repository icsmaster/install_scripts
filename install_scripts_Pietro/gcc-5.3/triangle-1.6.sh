#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"
GCC_VERSION_M="_gcc-5.3"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}

# NAME OF THE APPLICATION
APP_NAME="triangle"

# VERSION OF THE APPLICATION
APP_VERSION="1.6"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://www.netlib.org/voronoi/${APP_NAME}.zip"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}.zip"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
unzip ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

###############################################

# cd into installdir
cd ${INSTALLDIR}

# generate bin directory
mkdir -pv bin

# generate lib directory
mkdir -pv lib

# generate include directory
mkdir -pv include

# cd into extracted source directory
cd ${WORKDIR}

# make
make

# make library
make trilibrary

# ar
ar -r libtriangle.a triangle.o

# cd into extracted source directory
cd ${WORKDIR}

# bin
cp triangle ${INSTALLDIR}/bin
cp showme ${INSTALLDIR}/bin

# lib
cp libtriangle.a ${INSTALLDIR}/lib

# include
cp triangle.h ${INSTALLDIR}/include

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 TRIANGLE_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 CPATH	${INSTALLDIR}/include 
prepend-path	 PATH	${INSTALLDIR}/bin 
 
###############################################
EOF
