#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"
GCC_VERSION_M="_gcc-5.3"

# PKG-CONFIG VERSION
PKG-CONFIG_VERSION="0.29.1"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load pkg-config/${PKG-CONFIG}

# NAME OF THE APPLICATION
APP_NAME="swig"

# VERSION OF THE APPLICATION
APP_VERSION="3.0.10"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://downloads.sourceforge.net/${APP_NAME}/${APP_NAME}-${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xzf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" --prefix=${INSTALLDIR} --without-java \
			--without-perl5 --without-php --without-ruby --without-tcl "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

###############################################

# cd into extracted source directory
cd ${WORKDIR}/swig-3.0.10

# configure
./configure ${APP_FLAGS}

# make
make

# make install
make install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 SWIG_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path     PATH   ${INSTALLDIR}/bin 
 
###############################################
EOF
