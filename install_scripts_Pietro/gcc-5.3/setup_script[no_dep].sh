#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# ENVIRONMENT VARIABLES
# IN MODULE SECTION

# NAME OF THE APPLICATION
APP_NAME="cmake"

# VERSION OF THE APPLICATION
APP_VERSION="3.5.2"

# TYPE OF THE APPLICATION
APP_TYPE="compilers"

# ADDRESS OF THE REPOSITORY
APP_REPO="https://cmake.org/files/v${APP_VERSION%.*}/${APP_NAME}-${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.gz"

# CONFIGURATION
APP_CONF="${APP_NAME}-${APP_VERSION}/configure"

# FLAGS [!!! prefix, parallel(4 threads) & libs !!!]
APP_FLAGS="--prefix=${INSTALLDIR} --parallel=4 --no-system-libs --no-qt-gui"

# Notes
#   --parallel=n
#       bootstrap cmake in parallel, where n is
#       number of nodes [1]
#
#   --no-system-libs
#       use all system-installed third-party libraries
#       (for use only by package maintainers).
#
#   --no-qt-gui
#       do not build the Qt-based GUI (default)

###############################################

WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}"

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files
wget ${APP_REPO}

# extract downloaded file
tar xzf ${APP_FILE}

# create the build directory
mkdir -pv ${WORKDIR}/build
cd build

# configuration
../${APP_CONF} ${APP_FLAGS}

# make & install
make -j 8 && make install

# create module file
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

cat >${APP_VERSION} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##
proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

set CMAKE_DIR ${INSTALLDIR}

###############################################

prepend-path PATH  \$APP_DIR/bin
EOF
