#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"
GCC_VERSION_M="_gcc-5.3"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2_gcc-5.3.0"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}

# NAME OF THE APPLICATION
APP_NAME="scotch"

# VERSION OF THE APPLICATION
APP_VERSION="6.0.4"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://gforge.inria.fr/frs/download.php/file/34618/${APP_NAME}_${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}_${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf)
ARC_TYPE="xzf"

# CONFIGURATION
APP_CONF="${APP_NAME}-${APP_VERSION}/configure"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS=""

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

cd ${WORKDIR}/scotch_6.0.4/src

###############################################

cat <<EOF > Makefile.inc
EXE =
LIB = .a
OBJ = .o
MAKE = make
AR = ar
ARFLAGS = cr
CAT = cat
CCS = cc
CCP = mpicc
CCD = mpicc
PTHREAD ?=
CFLAGS  = -O3 -Drestrict=__restrict
CFLAGS += -DCOMMON_FILE_COMPRESS_GZ
CFLAGS += -DCOMMON_PTHREAD -DCOMMON_PTHREAD_BARRIER
CFLAGS += -DCOMMON_RANDOM_FIXED_SEED -DCOMMON_TIMING_OLD
CFLAGS += \$(PTHREAD) -DSCOTCH_RENAME
CLIBFLAGS = -fPIC
LDFLAGS = -lm -lz
CP = cp
LEX = flex -Pscotchyy -olex.yy.c
LN = ln
MKDIR = mkdir -p
MV = mv
RANLIB = echo
YACC = bison -pscotchyy -y -b y
EOF

###############################################

# make clean
make clean

# make
make PTHREAD=-DSCOTCH_PTHREAD scotch
make PTHREAD=-DSCOTCH_PTHREAD esmumps
make PTHREAD= ptscotch
make PTHREAD= ptesmumps

# check
make PTHREAD=-DSCOTCH_PTHREAD check
make PTHREAD= ptcheck

# remove old installation
for dir in {bin,lib,include}; do
  install -d ${INSTALLDIR}/$dir
  install -m 755 ${WORKDIR}/scotch_6.0.4/$dir/* ${INSTALLDIR}/$dir

done
rm ${INSTALLDIR}/{include,lib}/*metis*

# post
#cd ${INSTALLDIR}/lib
##export LINK=cc
#make_shlib libscotcherr.a ${APP_VERSION}
#make_shlib libscotcherrexit.a ${APP_VERSION}
#make_shlib libscotch.a ${APP_VERSION} \
#  -lz -lm -L. -lscotcherr
#make_shlib libesmumps.a ${APP_VERSION} \
#  -L. -lscotcherr -lscotch
#export LINK=mpicc
#make_shlib libptscotcherr.a ${APP_VERSION}
#make_shlib libptscotcherrexit.a ${APP_VERSION}
#make_shlib libptscotch.a ${APP_VERSION} \
#  -lz -lm -L. -lscotch -lptscotcherr
#make_shlib libptesmumps.a ${APP_VERSION} \
#  -L. -lscotch -lptscotcherr -lptscotch

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0
module load openmpi/1.10.2_gcc-5.3 

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-6.1 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

set SCOTCH_DIR ${INSTALLDIR}
setenv SCOTCH_DIR \$SCOTCH_DIR

###############################################
###	        	  SET PATHS:
###############################################

###############################################
EOF
