#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"
GCC_VERSION_M="_gcc-5.3"

# OPENBLAS VERSION
OPENBLAS_VERSION="0.2.18"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openblas/${OPENBLAS_VERSION}

# NAME OF THE APPLICATION
APP_NAME="suitesparse"

# VERSION OF THE APPLICATION
APP_VERSION="4.5.3"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://faculty.cse.tamu.edu/davis/SuiteSparse/SuiteSparse-${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="SuiteSparse-${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xzf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" "

###############################################

# mkdir -pv ${INSTALLDIR}/{include,lib}

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

CUDA=no

# INSTALL_LIB=${INSTALLDIR}/lib
# INSTALL_INCLUDE=${INSTALLDIR}/include

###############################################

# cd into extracted source directory
cd ${WORKDIR}/SuiteSparse

# make
make

# make install
make install
cp -r ${WORKDIR}/SuiteSparse/lib ${INSTALLDIR}
cp -r ${WORKDIR}/SuiteSparse/include ${INSTALLDIR}

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0
module load openblas/0.2.18_gcc-5.3

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}.
WARNING: This is NOT a CUDA-accelerated version! If you need CUDA, you can load suitesparse_CUDA module."

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 SUITESPARSE_DIR	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 CPATH	${INSTALLDIR}/include 

###############################################
EOF
