#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"
GCC_VERSION_M="_gcc-5.3"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2_gcc-5.3.0"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}

# NAME OF THE APPLICATION
APP_NAME="hypre"

# VERSION OF THE APPLICATION
APP_VERSION="2.11.1"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://computation.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods/download/${APP_NAME}-${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf)
ARC_TYPE="xzf"

# CONFIGURATION
APP_CONF="${APP_NAME}-${APP_VERSION}/configure"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS="CC="mpicc" \
    CXX="mpicxx" \
    CFLAGS="-O3 -fPIC" \
    CXXFLAGS="-O3 -fPIC" \
    --prefix=${INSTALLDIR} \
    --libdir=${INSTALLDIR}/lib \
    --with-blas=yes \
    --with-lapack=yes \
    --with-blas-libs="-framework Accelerate" \
    --with-lapack-libs="-framework Accelerate" \
    --without-babel \
    --without-mli \
    --without-fei \
    --without-superlu"

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}/src/

# configure
./configure ${APP_FLAGS}

cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}/src

# build
make

cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}/src

# install
#make install

install -d ${INSTALLDIR}/lib
install -d ${INSTALLDIR}/include
install -m 755 ${WORKDIR}/${APP_NAME}-${APP_VERSION}/src/hypre/lib/libHYPRE.a ${INSTALLDIR}/lib
install -m 644 ${WORKDIR}/${APP_NAME}-${APP_VERSION}/src/hypre/include/*.h ${INSTALLDIR}/include

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0
module load openmpi/1.10.2_gcc-5.3 

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-6.1 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

set HYPRE_DIR ${INSTALLDIR}
setenv HYPRE_DIR \$HYPRE_DIR

###############################################
###	        	  SET PATHS:
###############################################

###############################################
EOF
