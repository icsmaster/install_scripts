#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"
GCC_VERSION_M="_gcc-5.3"

# GMP VERSION
GMP_VERSION="6.1.0"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load gmp/${GMP_VERSION}

# NAME OF THE APPLICATION
APP_NAME="soplex"

# VERSION OF THE APPLICATION
APP_VERSION="2.2.1"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://soplex.zib.de/download/release/${APP_NAME}-${APP_VERSION}.tgz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tgz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xzf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

###############################################

# cd into installdir
cd ${INSTALLDIR}

# generate bin directory
#mkdir -pv bin

# generate lib directory
#mkdir -pv lib

# cd into extracted source directory
cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}

# make
make COMP=gnu OPT=opt

# make test
make COMP=gnu OPT=opt test

# cd into extracted source directory
cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}

# install
cp -r bin ${INSTALLDIR}
cp -r lib ${INSTALLDIR}

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0
module load gmp/6.1.0_gcc-5.3 

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0
conflict gmp/6.1.0_gcc-4.8      

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 SOPLEX_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 PATH	${INSTALLDIR}/bin 
 
###############################################
EOF
