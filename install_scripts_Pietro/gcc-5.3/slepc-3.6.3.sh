#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2_gcc-5.3.0"

# ARPACK VERSION
ARPACK_VERSION="3.2.0"

# PETSC VERSION
PETSC_VERSION="3.7.2"

# PYTHON VERSION
PYTHON_VERSION="3.5.0"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}
module load arpack/${ARPACK_VERSION}
module load petsc/${PETSC_VERSION}
module load python/${PYTHON_VERSION}

# NAME OF THE APPLICATION
APP_NAME="slepc"

# VERSION OF THE APPLICATION
APP_VERSION="3.6.3"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://slepc.upv.es/download/download.php?filename=${APP_NAME}-${APP_VERSION}.tar.gz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.gz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xzf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" --prefix=${INSTALLDIR} --with-clean=true --download-blopex \
			--with-arpack=1 --with-arpack-dir=${ARPACK_DIR}/lib --with-arpack-flags=-lparpack,-larpack "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget -O ${APP_NAME}-${APP_VERSION}.tar.gz ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

# SLEPC_DIR variable
export SLEPC_DIR=${WORKDIR}/${APP_NAME}-${APP_VERSION}
#export PETSC_ARCH=linux-gnu

# rename libraries
cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}/lib/slepc/conf
cp slepc_common slepccommon
cp slepc_rules slepcrules
cp slepc_variables slepcvariables
cp slepc_test slepctest

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77                                  
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'     

###############################################

# cd into extracted source directory
cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}

# build
make SLEPC_DIR=$PWD PETSC_DIR=$PETSC_DIR

# cd into extracted source directory
cd ${WORKDIR}/${APP_NAME}-${APP_VERSION}

# package
make SLEPC_DIR=$PWD PETSC_DIR=$PETSC_DIR install

# cd into src directory
cd ${WORKDIR}

# generate slepc4py directory
mkdir -pv slepc4py
cd slepc4py

# post_package (petsc4py)
curl -O -L https://bitbucket.org/slepc/slepc4py/downloads/slepc4py-3.6.0.tar.gz
tar xvfz slepc4py-3.6.0.tar.gz
mkdir -pv slepc4py-3.6.0
cd slepc4py-3.6.0
export SLEPC_DIR=${INSTALLDIR}
python setup.py install --prefix=${INSTALLDIR}

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##
proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 SLEPC_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 PATH	${INSTALLDIR}/bin 
prepend-path PKG_CONFIG_PATH ${INSTALLDIR}/lib/pkgconfig
prepend-path PYTHONPATH ${INSTALLDIR}/lib/python${PYTHON_VERSION}/site-packages
 
###############################################
EOF
