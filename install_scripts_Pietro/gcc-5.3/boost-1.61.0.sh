#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUIRED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"
GCC_VERSION_M="_gcc-5.3"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2_gcc-5.3"

# PYTHON VERSION
PYTHON_VERSION="3.5.0"

# REQUIRED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}
module load python/${PYTHON_VERSION}

# NAME OF THE APPLICATION
APP_NAME="boost"
DIR_NAME="BOOST"

# VERSION OF THE APPLICATION
APP_VERSION="1.61.0"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="https://sourceforge.net/projects/boost/files/latest/download?source=files"

# NAME OF THE DOWNLOADED FILE
APP_FILE="boost_1_61_0.tar.bz2"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xjf"

# DIRECTORIES
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"
WORKDIR="${INSTALLDIR}/src"
BLDDIR=${WORKDIR}/build
BINDIR=${INSTALLDIR}/bin
LIBDIR=${INSTALLDIR}/lib
INCDIR=${INSTALLDIR}/include

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" --prefix=${INSTALLDIR} --exec-prefix=${BINDIR} --includedir=${INCDIR} --libdir=${LIBDIR} "
			
###############################################

# generate directories
mkdir -pv ${WORKDIR}
mkdir -pv ${BLDDIR}
mkdir -pv ${BINDIR}
mkdir -pv ${LIBDIR}
mkdir -pv ${INCDIR}

# move to workdir
cd ${WORKDIR}

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget -O ${APP_FILE} ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

###############################################

# cd into extracted source directory
cd ${WORKDIR}/boost_1_61_0

# configure
./bootstrap.sh ${APP_FLAGS}

# build
./b2 ${APP_FLAGS} --build-dir=${BLDDIR} install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0
module load openmpi/1.10.2_gcc-5.3
module load python/3.5.0

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-6.1 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION} ${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv				${DIR_NAME}_DIR			${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path		PATH 					${BINDIR} 
prepend-path		LD_LIBRARY_PATH 		${LIBDIR} 
prepend-path		LIBRARY_PATH			${LIBDIR} 
prepend-path		INCLUDE_PATH       		${INCDIR}
prepend-path		CPATH					${INCDIR} 
 
###############################################
EOF