#! /bin/bash

# Created by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="5.3.0"
GCC_VERSION_M="_gcc-5.3"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2_gcc-5.3.0"

# SCOTCH VERSION
SCOTCH_VERSION="6.0.4"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}
module load scotch/${SCOTCH_VERSION}

# NAME OF THE APPLICATION
APP_NAME="pastix"

# VERSION OF THE APPLICATION
APP_VERSION="5.2.2.22"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="https://gforge.inria.fr/frs/download.php/file/35070/${APP_NAME}_${APP_VERSION}.tar.bz2"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}_${APP_VERSION}.tar.bz2"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xjf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix & libs !!!]
APP_FLAGS=" "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

# cd into extracted source directory
cd ${WORKDIR}/${APP_NAME}_${APP_VERSION}/src

###############################################
###				 EXPORT VARIABLES:
###############################################

###############################################

###############################################
###				CONFIG.IN
###############################################

  cat <<EOF > config.in
HOSTARCH    = i686_pc_linux
VERSIONBIT  = _64bit
EXEEXT      =
OBJEXT      = .o
LIBEXT      = .a
CCPROG      = cc
CFPROG      = gfortran
CF90PROG    = gfortran
MCFPROG     = mpif90
CF90CCPOPT  = -fpp
CCFOPT      = -O3
CCFDEB      = -g3
CXXOPT      = -O3
NVCCOPT     = -O3

LKFOPT      =
MKPROG      = make
MPCCPROG    = mpicc
MPCXXPROG   = mpic++
CPP         = cpp

ARFLAGS     = ruv
ARPROG      = ar
EXTRALIB    = -lifcore -lm -lrt
CTAGSPROG   = ctags

VERSIONMPI  = _mpi
VERSIONSMP  = _smp
VERSIONSCH  = _static
VERSIONINT  = _int
VERSIONPRC  = _simple
VERSIONFLT  = _real
VERSIONORD  = _scotch

ROOT       = ${INSTALLDIR}
INCLUDEDIR = \$(ROOT)/include
LIBDIR     = \$(ROOT)/lib
BINDIR     = \$(ROOT)/bin


SCOTCH_HOME ?= $SCOTCH_DIR
SCOTCH_INC ?= \$(SCOTCH_HOME)/include
SCOTCH_LIB ?= \$(SCOTCH_HOME)/lib
CCPASTIX   := \$(CCPASTIX) -I\$(SCOTCH_INC) -DDISTRIBUTED -DWITH_SCOTCH
EXTRALIB   := \$(EXTRALIB) -L\$(SCOTCH_LIB) -lptscotch -lscotch -lptscotcherrexit

EXTRALIB   := \$(EXTRALIB) -lpthread -L$GCC_DIR/lib -lgfortran

FOPT      := \$(CCFOPT)
FDEB      := \$(CCFDEB)
CCHEAD    := \$(CCPROG) \$(CCTYPES) \$(CCFOPT)
CCFOPT    := \$(CCFOPT) \$(CCTYPES) \$(CCPASTIX)
CCFDEB    := \$(CCFDEB) \$(CCTYPES) \$(CCPASTIX)
NVCCOPT   := \$(NVCCOPT) \$(CCTYPES) \$(CCPASTIX)

MAKE     = \$(MKPROG)
CC       = \$(MPCCPROG)
CFLAGS   = \$(CCFOPT) \$(CCTYPESFLT)
FC       = \$(MCFPROG)
FFLAGS   = \$(CCFOPT)
LDFLAGS  = \$(EXTRALIB) \$(BLASLIB)
EOF

###############################################

# cd into extracted source directory
cd ${WORKDIR}/${APP_NAME}_${APP_VERSION}/src

# make all
make all

# cd into extracted source directory
cd ${WORKDIR}/${APP_NAME}_${APP_VERSION}/src

# make install
make install

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}
##
## modulefiles/${APP_NAME}/${APP_VERSION}  Written by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/5.3.0
module load openmpi/1.10.2_gcc-5.3  
module load scotch/6.0.4_gcc-5.3

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/6.1.0
conflict openmpi/1.10.2_gcc-4.8  
conflict openmpi/1.10.2_gcc-6.1 

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION} - sets the Environment for ${APP_NAME} ${APP_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 PASTIX_DIR 	${INSTALLDIR}

###############################################
###	        	  PATHS:
###############################################

prepend-path	 CPATH	${INSTALLDIR}/include 
prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 PATH	${INSTALLDIR}/bin 
 
###############################################
EOF
