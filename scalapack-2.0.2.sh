#! /bin/bash

# created by Xiaozhou Li
# load gcc version you would like to compile this module with

SCALAPACK_VERSION="2.0.2"
GCC_VERSION="5.3.0"
WORKDIR="/apps/scalapack/${SCALAPACK_VERSION}/src"
INSTALLDIR="/apps/scalapack/${SCALAPACK_VERSION}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR

module load cmake
module load gcc/${GCC_VERSION}
module load openmpi


wget http://www.netlib.org/scalapack/scalapack-${SCALAPACK_VERSION}.tgz
tar zxvf scalapack-${SCALAPACK_VERSION}.tgz

# create the build directory
#mkdir -pv ${WORKDIR}/build
#cd build

# build with cmake for shared library

cd scalapack-${SCALAPACK_VERSION}
mkdir build
cd build 
cmake -D BUILD_SHARED_LIBS:BOOL=ON -D CMAKE_INSTALL_PREFIX=${INSTALLDIR} $WORKDIR/scalapack-${SCALAPACK_VERSION}
make -j 10
make install

##############################
#for module files
##############################
mkdir -pv /apps/Modules/3.2.10/modulefiles/scalapack
cd /apps/Modules/3.2.10/modulefiles/scalapack

cat >${SCALAPACK_VERSION}<<EOF
#%Module 3.2.0#####################################################################
#
# modules scalapack-${SCALAPACK_VERSION}
#
# modulefiles/scalapack/${SCALAPACK_VERSION}  Written by Xiaozhou Li
#
proc ModulesHelp {   } {
  global version modroot
      puts stderr "scalapack-${SCALAPACK_VERSION} - sets the Environment for
      SCALAPACK ${SCALAPACK_VERSION}"
}

module-whatis  "Sets the environment for using SCALAPACK-${SCALAPACK_VERSION}
built with gcc-${GCC_VERSION}"

set SCALAPACK_DIR ${INSTALLDIR}
setenv SCALAPACK_DIR               \$SCALAPACK_DIR

prepend-path LD_LIBRARY_PATH    \$SCALAPACK_DIR/lib

EOF
