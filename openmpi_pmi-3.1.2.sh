#! /bin/bash

# created by Radim Janalik

pkgname="openmpi"
pkgver="3.1.2"
pkgurl="https://www.open-mpi.org/software/ompi/v3.1/downloads/${pkgname}-${pkgver}.tar.bz2"
WORKDIR="/apps/${pkgname}/${pkgver}/src"
INSTALLDIR="/apps/${pkgname}/${pkgver}"

module load gcc
export CPATH=/usr/include/slurm

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
wget ${pkgurl}
tar xjf ${pkgname}-${pkgver}.tar.bz2

# create the build directory
mkdir -pv build
cd build

# build
../${pkgname}-${pkgver}/configure --prefix=${INSTALLDIR} --with-pmi=/usr/include/slurm --with-pmi-libdir=/usr/lib64
make -j 8 && make install

# Notes

mkdir -pv /apps/third-party/modulefiles/${pkgname}
cd /apps/third-party/modulefiles/${pkgname}
cat >${pkgver}_gcc-6.1<<EOF
#%Module 3.2.0#####################################################################
##
## modules openmpi-1.10.2
##
## modulefiles/openmpi/1.10.2  Written by Pietro Bressana
##

###############################################
###            CONFLICTING MODULES:
###############################################

conflict openmpi

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/6.1.0

###############################################


proc ModulesHelp {   } {
  global version modroot
    puts stderr "openmpi-3.1.2 - sets the Environment for openmpi 3.1.2"
}

module-whatis  "Sets the environment for using openmpi-3.1.2, built with gcc-6.1.0"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 OPENMPI_DIR 	$INSTALLDIR

###############################################
###	        	  SET PATHS:
###############################################

prepend-path	 LIBRARY_PATH	$INSTALLDIR/lib 
prepend-path	 LD_LIBRARY_PATH	$INSTALLDIR/lib 
prepend-path	 CPATH	$INSTALLDIR/include 
prepend-path	 PATH	$INSTALLDIR/bin 

###############################################
EOF
