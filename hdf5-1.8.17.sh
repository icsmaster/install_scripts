#! /bin/bash

# created by Hardik Kothari
# load gcc version you would like to compile this module with

HDF5_VERSION="1.8.17"
WORKDIR="/apps/hdf5/${HDF5_VERSION}/src"
INSTALLDIR="/apps/hdf5/${HDF5_VERSION}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR

wget https://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-${HDF5_VERSION}.tar.gz

tar xzf hdf5-${HDF5_VERSION}.tar.gz

export CXX=mpicxx
export CC=mpicc
export FC=mpif90
export F9X=mpif90
export RUNPARALLEL=mpirun
export OMPI_MCA_disable_memory_allocator=1

# create the build directory
mkdir -pv ${WORKDIR}/build
cd build

# build
../hdf5-${HDF5_VERSION}/configure --prefix=${INSTALLDIR} --enable-shared --enable-production --enable-parallel=yes --enable-cxx --enable-unsupported

# notes:
#   --enable-shared
#       disable/enable building shared hdf5 library
#   --enable-ipv6
#       Enable ipv6 (with ipv4) support
#   --with-ensurepip
#       install" or "upgrade" using bundled pip"
#   --without-gcc   
#       never use gcc

make -j 8 && make install

##############################
#for module files
##############################
mkdir -pv /apps/Modules/3.2.10/modulefiles/hdf5
cd /apps/Modules/3.2.10/modulefiles/hdf5

cat >${HDF5_VERSION}<<EOF
#%Module 3.2.0#####################################################################
#
# modules hdf5-${HDF5_VERSION}
#
# modulefiles/hdf5/${HDF5_VERSION}  Written by Hardik Kothari
#
proc ModulesHelp {   } {
  global version modroot
    puts stderr "hdf5-${HDF5_VERSION} - sets the Environment for HDF5 ${HDF5_VERSION}"
}

module-whatis  "Sets the environment for using HDF5-${HDF5_VERSION} built with gcc-${GCC_VERSION}"

set HDF5_DIR ${INSTALLDIR}

prepend-path PATH               \$HDF5_DIR/bin
prepend-path LD_LIBRARY_PATH    \$HDF5_DIR/lib
prepend-path INCLUDE_PATH       \$HDF5_DIR/include
EOF
