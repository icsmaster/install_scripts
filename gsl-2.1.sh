#! /bin/bash

# created by Radim Janalik

GSL_VERSION="2.1"
COMPILER="/apps/gcc/gcc-5.3.0/bin/gcc"
COMPILER_STR="gcc-5.3.0"
ROOTDIR="/apps/gsl/${COMPILER_STR}"
WORKDIR="${ROOTDIR}/${GSL_VERSION}/src"
INSTALLDIR="${ROOTDIR}/${GSL_VERSION}"

# load required modules
module load gcc/5.3.0

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
wget http://mirror.easyname.at/gnu/gsl/gsl-${GSL_VERSION}.tar.gz
tar -zxf gsl-${GSL_VERSION}.tar.gz

# cd to WORKDIR
cd $WORKDIR/gsl-${GSL_VERSION}

# build
mkdir -pv $WORKDIR/gsl-${GSL_VERSION}-build
cd $WORKDIR/gsl-${GSL_VERSION}-build
../gsl-${GSL_VERSION}/configure --prefix=${INSTALLDIR} CC=${COMPILER}
make
make install

# create module
mkdir -pv /apps/third-party/modulefiles/gsl
cd /apps/third-party/modulefiles/gsl/
cat >${GSL_VERSION} << EOF
#%Module 3.2.0#####################################################################
##
## module gsl-2.1
##
## modulefiles/gsl/2.1  Written by Radim Janalik
##

proc ModulesHelp { } { 
   global version modroot
      puts stderr "gsl-2.1 - sets the Environment for GSL 2.1"
      }

      module-whatis   "Sets the environment for using gsl-2.1"

      set GSL_VERSION 2.1
      set GSL_DIR /apps/gsl/gcc-5.3.0/${GSL_VERSION}

      prepend-path PATH       \$GSL_DIR/bin
      prepend-path MANPATH       \$GSL_DIR/man
      prepend-path LD_LIBRARY_PATH  \$GSL_DIR/lib
EOF
