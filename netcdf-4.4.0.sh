#! /bin/bash

# created by Hardik Kothari
# load gcc version you would like to compile this module with

NETCDF_VERSION="4.4.0"
WORKDIR="/apps/netcdf/${NETCDF_VERSION}/src"
INSTALLDIR="/apps/netcdf/${NETCDF_VERSION}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
module load openmpi
module load hdf5

export CC=mpicc
export FC=mpif90
export CXX=mpicxx
export CPPFLAGS="-I$HDF5_DIR/include"
export LDFLAGS="-L$HDF5_DIR/lib"

wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-${NETCDF_VERSION}.tar.gz
tar xzf netcdf-${NETCDF_VERSION}.tar.gz

# create the build directory
mkdir -pv ${WORKDIR}/build
cd build

# build
../netcdf-${NETCDF_VERSION}/configure --prefix=${INSTALLDIR} --enable-shared --enable-netcdf-4 

# notes:
#   --enable-shared
#       disable/enable building shared netcdf library
#   --enable-ipv6
#       Enable ipv6 (with ipv4) support
#   --with-ensurepip
#       install" or "upgrade" using bundled pip"
#   --without-gcc   
#       never use gcc

make -j 8 && make install

##############################
#for module files
##############################
mkdir -pv /apps/Modules/3.2.10/modulefiles/netcdf
cd /apps/Modules/3.2.10/modulefiles/netcdf

cat >${NETCDF_VERSION}<<EOF
#%Module 3.2.0#####################################################################
#
# modules netcdf-${NETCDF_VERSION}
#
# modulefiles/netcdf/${NETCDF_VERSION}  Written by Hardik Kothari
#
proc ModulesHelp {   } {
  global version modroot
    puts stderr "netcdf-${NETCDF_VERSION} - sets the Environment for NETCDF ${NETCDF_VERSION}"
}

module-whatis  "Sets the environment for using NETCDF-${NETCDF_VERSION} built with gcc-${GCC_VERSION}"

set NETCDF_DIR ${INSTALLDIR}
setenv NETCDF_DIR               \$NETCDF_DIR

prepend-path PATH               \$NETCDF_DIR/bin
prepend-path PKG_CONFIGP_PATH   \$NETCDF_DIR/lib/pkgconfig
prepend-path MANPATH            \$NETCDF_DIR/share/man
EOF
