#! /bin/bash

# created by Radim Janalik

LIKWID_VERSION="4.1.1"
ROOTDIR="/apps/likwid"
WORKDIR="/apps/likwid/${LIKWID_VERSION}/src"
INSTALLDIR="/apps/likwid/${LIKWID_VERSION}"

# load required modules
module load gcc/5.3.0

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
wget http://ftp.fau.de/pub/likwid/likwid-${LIKWID_VERSION}.tar.gz
tar -zxf likwid-${LIKWID_VERSION}.tar.gz

# cd to WORKDIR
cd $WORKDIR/likwid-${LIKWID_VERSION}

# adjust config.mk
# need to set PREFIX = $WORKDIR
#echo "You need first set PREFIX = $INSTALLDIR in $WORKDIR/likwid-${LIKWID_VERSION}/config.mk"
oldPath="/usr/local"
sed -e "s#PREFIX = $oldPath#PREFIX = $INSTALLDIR#" -i $WORKDIR/likwid-${LIKWID_VERSION}/config.mk

# build
make
sudo make install

# create module
mkdir -pv /apps/third-party/modulefiles/likwid
cd /apps/third-party/modulefiles/likwid/
cat >${LIKWID_VERSION} << EOF
#%Module 3.2.0#####################################################################
##
## module likwid-4.1.1
##
## modulefiles/likwid/4.1.1  Written by Radim Janalik
##

proc ModulesHelp { } { 
   global version modroot
      puts stderr "likwid-4.1.1 - sets the Environment for LIKWID 4.1.1"
      }

      module-whatis   "Sets the environment for using likwid-4.1.1"

      set LIKWID_VERSION 4.1.1
      set LIKWID_DIR /apps/likwid/${LIKWID_VERSION}

      prepend-path PATH       \$LIKWID_DIR/bin
      prepend-path MANPATH       \$LIKWID_DIR/man
      prepend-path LD_LIBRARY_PATH  \$LIKWID_DIR/lib
EOF