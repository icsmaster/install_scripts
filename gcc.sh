#! /bin/bash

# created by Hardik Kothari

pkgname="gcc"
pkgver="6.1.0"
pkgurl="http://ftpmirror.gnu.org/gcc/gcc-6.1.0/gcc-6.1.0.tar.bz2"
WORKDIR="/apps/${pkgname}/${pkgver}/src"
INSTALLDIR="/apps/${pkgname}/${pkgver}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
wget ${pkgurl}
tar xjf ${pkgname}-${pkgver}.tar.bz2

# download the prerequisites
cd ${pkgname}-${pkgver}
./contrib/download_prerequisites

# create the build directory
cd ..
mkdir -pv build
cd build

# build
../${pkgname}-${pkgver}/configure --prefix=${INSTALLDIR} --disable-multilib --enable-plugin --enable-lto -disable-nls --with-system-zlib --enable-languages=c,c++,fortran --enable-libstdcxx-time=yes --enable-checking=release
make -j 4 && make install

# Notes
#   --enable-shared
#       This option builds shared versionss of libraries
#
#   --disable-multilib
#       This parameter ensures that files are created for the specific
#       architecture of your computer.
#       This will disable building 32-bit support on 64-bit systems where the
#       32 bit version of libc is not installed and you do not want to go
#       through the trouble of building it. Diagnosis: "Compiler build fails
#       with fatal error: gnu/stubs-32.h: No such file or directory"
#
#   --with-system-zlib
#       Uses the system zlib instead of the bundled one. zlib is used for
#       compressing and uncompressing GCC's intermediate language in LTO (Link
#       Time Optimization) object files.
#
#   --enable-clocale=gnu
#       This parameter is a failsafe for incomplete locale data.
#
#   --enable-languages=all
#       This command identifies which languages to build. You may modify this
#       command to remove undesired language

cd /apps/Modules/3.2.10/modulefiles/${pkgname}
cat >${pkgver}<<EOF
#%Module 3.2.0#####################################################################
##
## modules ${pkgname}-${pkgver}
##
## modulefiles/${pkgname}/${pkgver}  Written by Hardik Kothari
##
proc ModulesHelp {    } {
  global version modroot
      puts stderr "${pkgname}-${pkgver} - sets the Environment for GCC ${pgkver} in my home directory"
}

module-whatis  "Sets the environment for using ${pkgname}-${pkgver} compilers (C,,C++,Fortran)"

set GCC_DIR ${INSTALDIR}

prepend-path PATH               \$GCC_DIR/bin
prepend-path MANPATH            \$GCC_DIR/share/man
prepend-path INFOPATH           \$GCC_DIR/share/info
prepend-path LD_LIBRARY_PATH    \$GCC_DIR/lib64
EOF
