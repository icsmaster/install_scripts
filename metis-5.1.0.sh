#! /bin/bash

# created by Hardik Kothari
# load gcc version you would like to compile this module with

METIS_VERSION="5.1.0"
GCC_VERSION="5.3.0"
WORKDIR="/apps/metis/${METIS_VERSION}/src"
INSTALLDIR="/apps/metis/${METIS_VERSION}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
module load cmake
module load gcc/${GCC_VERSION}

export CC=gcc
export CXX=g++
export CPPFLAGS="-I$HDF5_DIR/include"
export LDFLAGS="-L$HDF5_DIR/lib"

wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-${METIS_VERSION}.tar.gz
tar xzf metis-${METIS_VERSION}.tar.gz

# create the build directory
#mkdir -pv ${WORKDIR}/build
#cd build

# build
#../metis-${METIS_VERSION}/configure --prefix=${INSTALLDIR} --enable-shared --enable-metis-4 

# notes:
#   --enable-shared
#       disable/enable building shared metis library
#   --enable-ipv6
#       Enable ipv6 (with ipv4) support
#   --with-ensurepip
#       install" or "upgrade" using bundled pip"
#   --without-gcc   
#       never use gcc

cd metis-${METIS_VERSION}
make config prefix=${INSTALLDIR} shared=1 
make
make install

##############################
#for module files
##############################
mkdir -pv /apps/Modules/3.2.10/modulefiles/metis
cd /apps/Modules/3.2.10/modulefiles/metis

cat >${METIS_VERSION}<<EOF
#%Module 3.2.0#####################################################################
#
# modules metis-${METIS_VERSION}
#
# modulefiles/metis/${METIS_VERSION}  Written by Hardik Kothari
#
proc ModulesHelp {   } {
  global version modroot
    puts stderr "metis-${METIS_VERSION} - sets the Environment for METIS ${METIS_VERSION}"
}

module-whatis  "Sets the environment for using METIS-${METIS_VERSION} built with gcc-${GCC_VERSION}"

set METIS_DIR ${INSTALLDIR}
setenv METIS_DIR               \$METIS_DIR

prepend-path PATH               \$METIS_DIR/bin
prepend-path LD_LIBRARY_PATH    \$METIS_DIR/lib

EOF
