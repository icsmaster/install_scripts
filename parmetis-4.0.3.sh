#! /bin/bash

# created by Hardik Kothari
# load gcc version you would like to compile this module with

PARMETIS_VERSION="4.0.3"
GCC_VERSION="5.3.0"
WORKDIR="/apps/parmetis/${PARMETIS_VERSION}/src"
INSTALLDIR="/apps/parmetis/${PARMETIS_VERSION}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
module load cmake
module load gcc/${GCC_VERSION}
module load openmpi

export CC=mpicc
export CXX=mpicxx

export CPPFLAGS="-I$HDF5_DIR/include"
export LDFLAGS="-L$HDF5_DIR/lib"

wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/parmetis/parmetis-${PARMETIS_VERSION}.tar.gz
tar xzf parmetis-${PARMETIS_VERSION}.tar.gz

# create the build directory
#mkdir -pv ${WORKDIR}/build
#cd build

# build
#../parmetis-${PARMETIS_VERSION}/configure --prefix=${INSTALLDIR} --enable-shared --enable-parmetis-4 

# notes:
#   --enable-shared
#       disable/enable building shared parmetis library
#   --enable-ipv6
#       Enable ipv6 (with ipv4) support
#   --with-ensurepip
#       install" or "upgrade" using bundled pip"
#   --without-gcc   
#       never use gcc

cd parmetis-${PARMETIS_VERSION}
make config prefix=${INSTALLDIR} shared=1 
make
make install

##############################
#for module files
##############################
mkdir -pv /apps/Modules/3.2.10/modulefiles/parmetis
cd /apps/Modules/3.2.10/modulefiles/parmetis

cat >${PARMETIS_VERSION}<<EOF
#%Module 3.2.0#####################################################################
#
# modules parmetis-${PARMETIS_VERSION}
#
# modulefiles/parmetis/${PARMETIS_VERSION}  Written by Hardik Kothari
#
proc ModulesHelp {   } {
  global version modroot
    puts stderr "parmetis-${PARMETIS_VERSION} - sets the Environment for PARMETIS ${PARMETIS_VERSION}"
}

module-whatis  "Sets the environment for using PARMETIS-${PARMETIS_VERSION} built with gcc-${GCC_VERSION}"

set PARMETIS_DIR ${INSTALLDIR}
setenv PARMETIS_DIR               \$PARMETIS_DIR

prepend-path PATH               \$PARMETIS_DIR/bin
prepend-path LD_LIBRARY_PATH    \$PARMETIS_DIR/lib

EOF
