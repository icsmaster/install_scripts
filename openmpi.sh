#! /bin/bash

# created by Hardik Kothari

pkgname="openmpi"
pkgver="1.10.2"
pkgurl="https://www.open-mpi.org/software/ompi/v1.10/downloads/${pkgname}-${pkgver}.tar.bz2"
WORKDIR="/apps/${pkgname}/${pkgver}/src"
INSTALLDIR="/apps/${pkgname}/${pkgver}"

module load gcc

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
curl -O ${pkgurl}
tar xjf ${pkgname}-${pkgver}.tar.bz2

# create the build directory
mkdir -pv build
cd build

# build
../${pkgname}-${pkgver}/configure --prefix=${INSTALLDIR} --enable-mpi-fortran=all 
make -j 4 && make install

# Notes

cd /apps/Modules/3.2.10/modulefiles/${pkgname}
cat >${pkgver}<<EOF
#%Module 3.2.0#####################################################################
##
## modules ${pkgname}-${pkgver}
##
## modulefiles/${pkgname}/${pkgver}  Written by Hardik Kothari
##
proc ModulesHelp {    } {
  global version modroot
      puts stderr "${pkgname}-${pkgver} - sets the Environment for OPENMPI ${pgkver} in my home directory"
}

module-whatis  "Sets the environment for using ${pkgname}-${pkgver} compilers (C,,C++,Fortran)"

set OPENMPI_DIR ${INSTALDIR}

prepend-path PATH               \$OPENMPI_DIR/bin
prepend-path MANPATH            \$OPENMPI_DIR/share/man
prepend-path INFOPATH           \$OPENMPI_DIR/share/info
prepend-path LD_LIBRARY_PATH    \$OPENMPI_DIR/lib64
EOF
