#! /bin/bash

# created by Radim Janalik
# based on version 3.5 by Hardik Kothari

# load gcc version you would like to compile this module with
module load gcc/6.1.0

PYTHON_VERSION="2.7.12"
WORKDIR="/apps/python/${PYTHON_VERSION}/src"
INSTALLDIR="/apps/python/${PYTHON_VERSION}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR

wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tgz
tar xzf Python-${PYTHON_VERSION}.tgz

# create the build directory
mkdir -pv ${WORKDIR}/build
cd build

# build
../Python-${PYTHON_VERSION}/configure --prefix=${INSTALLDIR} --enable-shared --enable-ipv6 --with-ensurepip --without-gcc 

# notes:
#   --enable-shared
#       disable/enable building shared python library
#   --enable-ipv6
#       Enable ipv6 (with ipv4) support
#   --with-ensurepip
#       install" or "upgrade" using bundled pip"
#   --without-gcc   
#       never use gcc

make -j 8 && make install

##############################
#for module files
##############################
mkdir -pv /apps/third-party/modulefiles/python
cd /apps/third-party/modulefiles/python

cat >${PYTHON_VERSION}<<EOF
#%Module 3.2.0#####################################################################
#
# modules python-${PYTHON_VERSION}
#
# modulefiles/python/${PYTHON_VERSION}  Written by Hardik Kothari
#
proc ModulesHelp {   } {
  global version modroot
    puts stderr "python-${PYTHON_VERSION} - sets the Environment for PYTHON ${PYTHON_VERSION}"
}

module-whatis  "Sets the environment for using PYTHON-${PYTHON_VERSION} built with gcc-${GCC_VERSION}"

set PYTHON_DIR ${INSTALLDIR}

prepend-path PATH               \$PYTHON_DIR/bin
prepend-path LD_LIBRARY_PATH    \$PYTHON_DIR/lib
prepend-path INCLUDE_PATH       \$PYTHON_DIR/include
prepend-path MANPATH            \$PYTHON_DIR/share/man
EOF
