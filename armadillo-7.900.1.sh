#! /bin/bash

# Created by Radim Janalik
# based on version 7.400.2 by Pietro Bressana
# Please load the gcc version you would like to compile this module with

###############################################
###	        REQUESTED INFORMATION:
###############################################

# SET ENVIRONMENT VARIABLES
# IN MODULE SECTION

# GCC VERSION (default: 5.3.0)
GCC_VERSION="6.1.0"
GCC_VERSION_M="_gcc-6.1"

# OPENMPI VERSION
OPENMPI_VERSION="1.10.2${GCC_VERSION_M}"

# CMAKE VERSION
CMAKE_VERSION="3.5.2"

# ARPACK VERSION
ARPACK_VERSION="3.2.0${GCC_VERSION_M}"

# SUPERLU VERSION
SUPERLU_VERSION="5.2.1${GCC_VERSION_M}"

# HDF5 VERSION
HDF5_VERSION="1.8.17${GCC_VERSION_M}"

# OPENBLAS VERSION
OPENBLAS_VERSION="0.2.19${GCC_VERSION_M}_openmp"

# REQUESTED MODULES:
module load gcc/${GCC_VERSION}
module load openmpi/${OPENMPI_VERSION}
module load cmake/${CMAKE_VERSION}
module load arpack/${ARPACK_VERSION}
module load superlu/${SUPERLU_VERSION}
module load hdf5/${HDF5_VERSION}
module load openblas/${OPENBLAS_VERSION}

# NAME OF THE APPLICATION
APP_NAME="armadillo"

# VERSION OF THE APPLICATION
APP_VERSION="7.900.1"

# TYPE OF THE APPLICATION
APP_TYPE="third-party"

# ADDRESS OF THE REPOSITORY
APP_REPO="http://sourceforge.net/projects/arma/files/${APP_NAME}-${APP_VERSION}.tar.xz"

# NAME OF THE DOWNLOADED FILE
APP_FILE="${APP_NAME}-${APP_VERSION}.tar.xz"

# ARCHIVE TYPE (gzip: xzf / bzip2: xjf / other: xf)
ARC_TYPE="xf"

# DIRECTORIES
WORKDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/${APP_NAME}/${APP_VERSION}/gcc-${GCC_VERSION}"

# FLAGS [!!! prefix, parallel(12 threads) & libs !!!]
APP_FLAGS=" -DCMAKE_INSTALL_PREFIX=${INSTALLDIR} \
      -DCMAKE_BUILD_TYPE=None \
      -DCMAKE_FIND_FRAMEWORK=LAST \
      -Wno-dev "

###############################################

# generate the work directory
mkdir -pv $WORKDIR
cd $WORKDIR

# download setup files (if not yet downloaded)
if [ ! -f ${APP_FILE} ]; then
wget ${APP_REPO}
fi

# extract downloaded file
tar ${ARC_TYPE} ${APP_FILE}

###############################################
###				 EXPORT VARIABLES:
###############################################

CC=mpicc
CXX=mpicxx
FC=mpif90
F77=mpif77                                  
F90=mpif90                                    
CFLAGS='-fPIC -fopenmp'
CXXFLAGS='-fPIC -fopenmp'
FFLAGS='-fPIC -fopenmp'
FCFLAGS='-fPIC -fopenmp'
F90FLAGS='-fPIC -fopenmp'
F77FLAGS='-fPIC -fopenmp'        

###############################################

cd ${WORKDIR}/armadillo-7.900.1

# cmake
cmake ${APP_FLAGS}

# install
make install

#examples
#prefix.install "examples"

# create module file directory
mkdir -pv /apps/${APP_TYPE}/modulefiles/${APP_NAME}
cd /apps/${APP_TYPE}/modulefiles/${APP_NAME}

# create module file
cat >${APP_VERSION}${GCC_VERSION_M} <<EOF
#%Module 3.2.0#####################################################################
##
## modules ${APP_NAME}-${APP_VERSION}${GCC_VERSION_M}
##
## modulefiles/${APP_NAME}/${APP_VERSION}${GCC_VERSION_M}  Written by Radim Janalik
## based on version 7.400.2 by Pietro Bressana
##

###############################################
###           LOAD REQUIRED MODULES:
###############################################

module load gcc/${GCC_VERSION}
module load openmpi/1.10.2${GCC_VERSION_M}
module load cmake/3.5.2
module load arpack/3.2.0${GCC_VERSION_M}
module load superlu/5.2.1${GCC_VERSION_M}
module load hdf5/1.8.17${GCC_VERSION_M}
module load openblas/0.2.19${GCC_VERSION_M}_openmp

###############################################
###            CONFLICTING MODULES:
###############################################

conflict gcc/5.3.0
conflict openmpi/1.10.2_gcc-4.8
conflict openmpi/1.10.2_gcc-5.3

###############################################

proc ModulesHelp {   } {
  global version modroot
    puts stderr "${APP_NAME}-${APP_VERSION}${GCC_VERSION_M} - sets the Environment for ${APP_NAME} ${APP_VERSION}${GCC_VERSION_M} in my home directory"
}

module-whatis  "Sets the environment for using ${APP_NAME}-${APP_VERSION}, built with gcc-${GCC_VERSION}"

###############################################
###	        ENVIRONMENT VARIABLES:
###############################################

setenv			 ARMADILLO_DIR 	${INSTALLDIR}

###############################################
###	        	  SET PATHS:
###############################################

prepend-path	 CPATH			${INSTALLDIR}/include 
prepend-path	 LIBRARY_PATH	${INSTALLDIR}/lib 
prepend-path	 LD_LIBRARY_PATH	${INSTALLDIR}/lib 

###############################################
EOF
