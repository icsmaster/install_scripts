#! /bin/bash

# created by Hardik Kothari

GCC_VERSION="5.3.0"
WORKDIR="/apps/gcc/gcc-${GCC_VERSION}/src"
INSTALLDIR="/apps/gcc/gcc-${GCC_VERSION}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
wget http://www.netgull.com/gcc/releases/gcc-${GCC_VERSION}/gcc-${GCC_VERSION}.tar.bz2
tar jxf gcc-${GCC_VERSION}.tar.bz2

# download the prerequisites
cd gcc-${GCC_VERSION}
./contrib/download_prerequisites

# create the build directory
cd ..
mkdir -pv gcc-build
cd gcc-build

# build
../gcc-${GCC_VERSION}/configure --prefix=${INSTALLDIR} --enable-shared --disable-multilib --with-system-zlib --enable-clocale=gnu --enable-languages=all
make -j 8 && make install

# Notes
#   --enable-shared
#       This option builds shared versionss of libraries
#
#
#   --disable-multilib
#       This parameter ensures that files are created for the specific
#       architecture of your computer.
#        This will disable building 32-bit support on 64-bit systems where the
#        32 bit version of libc is not installed and you do not want to go
#        through the trouble of building it. Diagnosis: "Compiler build fails
#        with fatal error: gnu/stubs-32.h: No such file or directory"
#
#   --with-system-zlib
#       Uses the system zlib instead of the bundled one. zlib is used for
#       compressing and uncompressing GCC's intermediate language in LTO (Link
#       Time Optimization) object files.
#
#   --enable-clocale=gnu
#       This parameter is a failsafe for incomplete locale data.
#
#   --enable-languages=all
#       This command identifies which languages to build. You may modify this
#       command to remove undesired language

cd /apps/Modules/3.2.10/modulefiles/gcc
cat >${GCC_VERSION} <<EOF
#%Module 3.2.0#####################################################################
##
## modules gcc-${GCC_VERSION}
##
## modulefiles/gcc/gcc-${GCC_VERSION}  Written by Hardik Kothari
##
proc ModulesHelp {   } {
  global version modroot
    puts stderr "gcc-${GCC_VERSION} - sets the Environment for GCC ${GCC_VERSION} in my home directory"

}

module-whatis  "Sets the environment for using gcc-${GCC_VERSION} compilers (C,,C++,Fortran)"

set GCC_DIR /apps/gcc/gcc-${GCC_VERSION}

prepend-path PATH               \$GCC_DIR/bin
prepend-path MANPATH            \$GCC_DIR/share/man
prepend-path INFOPATH           \$GCC_DIR/share/info
prepend-path LD_LIBRARY_PATH    \$GCC_DIR/lib64
EOF