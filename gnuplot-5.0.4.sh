#! /bin/bash

# created by Radim Janalik

GNUPLOT_VERSION="5.0.4"
WORKDIR="/apps/gnuplot/${GNUPLOT_VERSION}/src"
INSTALLDIR="/apps/gnuplot/${GNUPLOT_VERSION}"

# load gcc 6.1.0
module load gcc/6.1.0

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR
if [ ! -f gnuplot-${GNUPLOT_VERSION}.tar.gz ]; then 
   wget https://sourceforge.net/projects/gnuplot/files/gnuplot/${GNUPLOT_VERSION}/gnuplot-${GNUPLOT_VERSION}.tar.gz
fi
tar -zxf gnuplot-${GNUPLOT_VERSION}.tar.gz

# create the build directory
mkdir -pv build
cd build

# build
../gnuplot-${GNUPLOT_VERSION}/configure --prefix=${INSTALLDIR}
make -j 8 && make install

mkdir -pv /apps/third-party/modulefiles/gnuplot
cd /apps/third-party/modulefiles/gnuplot
cat >${GNUPLOT_VERSION}<<EOF
#%Module 3.2.0#####################################################################
##
## modules gnuplot-${GNUPLOT_VERSION}
##
## modulefiles/gcc/gnuplot-${GNUPLOT_VERSION}  Written by Radim Janalik
##
proc ModulesHelp {   } {
  global version modroot
    puts stderr "gnuplot-${GNUPLOT_VERSION} - sets the Environment for gnuplot ${GNUPLOT_VERSION}"

}

module-whatis  "Sets the environment for using gnuplot-${GNUPLOT_VERSION}"

set GNUPLOT_DIR /apps/gnuplot/${GNUPLOT_VERSION}

prepend-path PATH               \$GNUPLOT_DIR/bin
prepend-path MANPATH            \$GNUPLOT_DIR/share/man
EOF
