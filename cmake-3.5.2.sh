#! /bin/bash

# created by Hardik Kothari
# load gcc version you would like to compile this module with

CMAKE_VERSION="3.5.2"

WORKDIR="/apps/cmake/cmake-${CMAKE_VERSION}/src"
INSTALLDIR="/apps/cmake/cmake-${CMAKE_VERSION}"

# get the source code
mkdir -pv $WORKDIR
cd $WORKDIR

wget https://cmake.org/files/v${CMAKE_VERSION%.*}/cmake-${CMAKE_VERSION}.tar.gz

tar xzf cmake-${CMAKE_VERSION}.tar.gz

# create the build directory
mkdir -pv ${WORKDIR}/cmake-build
cd cmake-build

# build
../cmake-${CMAKE_VERSION}/configure --prefix=${INSTALLDIR} --parallel=8 --no-system-libs --no-qt-gui
make -j 8 && make install

# Notes
#   --parallel=n
#       bootstrap cmake in parallel, where n is
#       number of nodes [1]
#
#   --no-system-libs
#       use all system-installed third-party libraries
#       (for use only by package maintainers).
#
#   --no-qt-gui
#       do not build the Qt-based GUI (default)

mkdir -pv /apps/Modules/3.2.10/modulefiles/cmake
cd /apps/Modules/3.2.10/modulefiles/cmake

cat >${CMAKE_VERSION} <<EOF
#%Module 3.2.0#####################################################################
##
## modules cmake-${CMAKE_VERSION}
##
## modulefiles/cmake/cmake-${CMAKE_VERSION}  Written by Hardik Kothari
##
proc ModulesHelp {   } {
  global version modroot
    puts stderr "cmake-${CMAKE_VERSION} - sets the Environment for cmake ${cmake_VERSION} in my home directory"
}

module-whatis  "Sets the environment for using cmake-${CMAKE_VERSION}"

set CMAKE_DIR ${INSTALLDIR}

prepend-path PATH  \$CMAKE_DIR/bin
EOF
